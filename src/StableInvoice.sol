// SPDX-License-Identifier: MIT
pragma solidity >=0.8.19;

import "solady/auth/Ownable.sol";
import "forge-std/interfaces/IERC20.sol";
import "forge-std/console.sol";

error InvalidSignature();
error UnsupportedToken();
error InvoiceExpired();

interface IUSDT {
    function owner() external returns (address);
    function balanceOf(address who) external returns (uint256);
    function approve(address _sender, uint256 _value) external;
    function transferFrom(address _from, address _to, uint256 _value) external;
    function issue(uint256 amount) external;
}

contract StableInvoice is Ownable {
    /// @dev emitted when a payment is successfully executed
    event InvoicePaid(address indexed user, address indexed token, uint256 amount);

    address public invoiceIssuer;
    address public payoutAddress;

    address public immutable usdc;
    address public immutable usdt;

    constructor(address _invoiceIssuer, address _payoutAddress, address _usdc, address _usdt) {
        invoiceIssuer = _invoiceIssuer;
        payoutAddress = _payoutAddress;
        usdc = _usdc;
        usdt = _usdt;

        _initializeOwner(msg.sender);
    }

    /// @dev process an invoice issed by invoiceIssuer
    /// @dev takes payment details as well as the r,s,v of an ecdsa signature
    function payInvoice(
        string calldata id,
        uint256 amount,
        address token,
        uint256 expiryBlock,
        bytes32 r,
        bytes32 s,
        uint8 v
    ) public {
        // reconstruct signature hash for comparison
        bytes memory messageHash = abi.encode(msg.sender, id, amount, token, expiryBlock);
        bytes32 signatureHash =
            keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n", toString(messageHash.length), messageHash));

        // recover signing address
        address recovered = ecrecover(signatureHash, v, r, s);

        // check satifies both that the hash of args matches signature,
        // and that the signer is the invoice issuer
        if (recovered != invoiceIssuer) {
            revert InvalidSignature();
        }

        if (block.number >= expiryBlock) {
            revert InvoiceExpired();
        }

        // execute token transfer
        if (token == usdc) {
            IERC20(usdc).transferFrom(msg.sender, payoutAddress, amount);
        } else if (token == usdt) {
            IUSDT(usdt).transferFrom(msg.sender, payoutAddress, amount);
        } else {
            // contract only supports USDC and USDT
            revert UnsupportedToken();
        }

        // payment succeeded, emit event
        emit InvoicePaid(msg.sender, token, amount);
    }

    function setInvoiceIssuer(address _invoiceIssuer) public onlyOwner {
        invoiceIssuer = _invoiceIssuer;
    }

    function setPayoutAddress(address _payoutAddress) public onlyOwner {
        payoutAddress = _payoutAddress;
    }

    /// @dev from vectorized/solady
    /// @dev Returns the base 10 decimal representation of `value`.
    function toString(uint256 value) internal pure returns (string memory str) {
        /// @solidity memory-safe-assembly
        assembly {
            // The maximum value of a uint256 contains 78 digits (1 byte per digit), but
            // we allocate 0xa0 bytes to keep the free memory pointer 32-byte word aligned.
            // We will need 1 word for the trailing zeros padding, 1 word for the length,
            // and 3 words for a maximum of 78 digits.
            str := add(mload(0x40), 0x80)
            // Update the free memory pointer to allocate.
            mstore(0x40, add(str, 0x20))
            // Zeroize the slot after the string.
            mstore(str, 0)

            // Cache the end of the memory to calculate the length later.
            let end := str

            let w := not(0) // Tsk.
            // We write the string from rightmost digit to leftmost digit.
            // The following is essentially a do-while loop that also handles the zero case.
            for { let temp := value } 1 {} {
                str := add(str, w) // `sub(str, 1)`.
                // Write the character to the pointer.
                // The ASCII index of the '0' character is 48.
                mstore8(str, add(48, mod(temp, 10)))
                // Keep dividing `temp` until zero.
                temp := div(temp, 10)
                if iszero(temp) { break }
            }

            let length := sub(end, str)
            // Move the pointer 32 bytes leftwards to make room for the length.
            str := sub(str, 0x20)
            // Store the length.
            mstore(str, length)
        }
    }
}
