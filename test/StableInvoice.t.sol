// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.19;

import "forge-std/Test.sol";
import "../src/StableInvoice.sol";

import "forge-std/interfaces/IERC20.sol";

import "forge-std/console.sol";

interface ITetherToken {
    function owner() external returns (address);
    function balanceOf(address who) external returns (uint256);
    function approve(address _sender, uint256 _value) external;
    function transfer(address _to, uint256 _value) external;
    function issue(uint256 amount) external;
}

interface IUnitedStatesDollarCoin is IERC20 {
    function masterMinter() external returns (address);
    function configureMinter(address minter, uint256 minterAllowedAmount) external returns (bool);
    function mint(address _to, uint256 _amount) external returns (bool);
}

contract StableInvoiceTest is Test {
    StableInvoice public stableInvoice;

    uint256 mainnetFork;

    address immutable usdcMainnetAddress = 0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48;
    address immutable usdtMainnetAddress = 0xdAC17F958D2ee523a2206206994597C13D831ec7;

    address public invoiceIssuer = 0xB7E7Ad44Edf69bFddCB7f1b5FC086f043Cbe97B3;

    function setUp() public {
        string memory MAINNET_RPC_URL = vm.envString("MAINNET_RPC_URL");
        mainnetFork = vm.createFork(MAINNET_RPC_URL);
    }

    function testTokenAddressSetInConstructor() public {
        stableInvoice = new StableInvoice(
            invoiceIssuer, // invoice issuing address
            invoiceIssuer, // payout address
            usdcMainnetAddress,
            usdtMainnetAddress 
        );

        assertEq(stableInvoice.usdc(), usdcMainnetAddress);
        assertEq(stableInvoice.usdt(), usdtMainnetAddress);
    }

    function testPayInvoiceUSDT() public {
        vm.selectFork(mainnetFork);

        stableInvoice = new StableInvoice(
            invoiceIssuer, // invoice issuing address
            invoiceIssuer, // payout address
            usdcMainnetAddress,
            usdtMainnetAddress 
        );

        uint256 INVOICE_ISSUER_PK = vm.envUint("INVOICE_ISSUER_PK");

        // address that will pay the invoiced amount
        address payer = vm.addr(1);
        string memory id = "9f615c06-7da7-4c65-aef7-946fa073b6b3";
        uint256 amount = 1000;
        address token = usdtMainnetAddress;
        uint256 expiryBlock = block.number + 100;

        // create invoice signature
        bytes memory hash = abi.encode(payer, id, amount, token, expiryBlock);
        bytes32 signatureHash =
            keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n", vm.toString(hash.length), hash));
        (uint8 v, bytes32 r, bytes32 s) = vm.sign(INVOICE_ISSUER_PK, signatureHash);

        // masquerade as tether owner and print to test address
        ITetherToken tether = ITetherToken(usdtMainnetAddress);
        address paolo = tether.owner();
        vm.startPrank(paolo);
        tether.issue(10000000000);
        tether.transfer(payer, 10000000000);
        vm.stopPrank();

        vm.startPrank(payer);

        // execute invoice payment on contract
        tether.approve(address(stableInvoice), 1000);
        stableInvoice.payInvoice(id, amount, token, expiryBlock, r, s, v);

        // test payout address received funds
        assertEq(tether.balanceOf(invoiceIssuer), 1000);
    }

    function testPayInvoiceUSDC() public {
        vm.selectFork(mainnetFork);

        stableInvoice = new StableInvoice(
            invoiceIssuer, // invoice issuing address
            invoiceIssuer, // payout address
            usdcMainnetAddress,
            usdtMainnetAddress 
        );

        uint256 INVOICE_ISSUER_PK = vm.envUint("INVOICE_ISSUER_PK");

        // address that will pay the invoiced amount
        address payer = vm.addr(1);
        string memory id = "9f615c06-7da7-4c65-aef7-946fa073b6b3";
        uint256 amount = 1000;
        address token = usdcMainnetAddress;
        uint256 expiryBlock = block.number + 100;

        // create invoice signature
        bytes memory hash = abi.encode(payer, id, amount, token, expiryBlock);
        bytes32 signatureHash =
            keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n", vm.toString(hash.length), hash));
        (uint8 v, bytes32 r, bytes32 s) = vm.sign(INVOICE_ISSUER_PK, signatureHash);

        // add a generated address to USDC minters list
        address fakeUsdcMinter = vm.addr(99);
        IUnitedStatesDollarCoin usdc = IUnitedStatesDollarCoin(usdcMainnetAddress);
        vm.prank(usdc.masterMinter());
        usdc.configureMinter(fakeUsdcMinter, 10000);
        // mint USDC to payer
        vm.prank(fakeUsdcMinter);
        usdc.mint(payer, 1000);

        vm.startPrank(payer);
        // execute invoice payment on contract
        usdc.approve(address(stableInvoice), 1000);
        stableInvoice.payInvoice(id, amount, token, expiryBlock, r, s, v);

        // test payout address received funds
        assertEq(usdc.balanceOf(invoiceIssuer), 1000);
    }

    function testBadSignerRevert() public {
        vm.selectFork(mainnetFork);

        stableInvoice = new StableInvoice(
            invoiceIssuer, // invoice issuing address
            invoiceIssuer, // payout address
            usdcMainnetAddress,
            usdtMainnetAddress 
        );

        uint256 fakeIssuerPK = 100;

        // address that will pay the invoiced amount
        address payer = vm.addr(1);
        string memory id = "9f615c06-7da7-4c65-aef7-946fa073b6b3";
        uint256 amount = 1000;
        address token = usdcMainnetAddress;
        uint256 expiryBlock = block.number + 100;

        // create invoice signature
        bytes memory hash = abi.encode(payer, id, amount, token, expiryBlock);
        bytes32 signatureHash =
            keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n", vm.toString(hash.length), hash));
        // NOT the invoiceIssuer
        (uint8 v, bytes32 r, bytes32 s) = vm.sign(fakeIssuerPK, signatureHash);

        vm.startPrank(payer);
        vm.expectRevert(InvalidSignature.selector);
        stableInvoice.payInvoice(id, amount, token, expiryBlock, r, s, v);
    }

    function testExpiredInvoiceRevert() public {
        vm.selectFork(mainnetFork);

        stableInvoice = new StableInvoice(
            invoiceIssuer, // invoice issuing address
            invoiceIssuer, // payout address
            usdcMainnetAddress,
            usdtMainnetAddress 
        );

        uint256 INVOICE_ISSUER_PK = vm.envUint("INVOICE_ISSUER_PK");

        // address that will pay the invoiced amount
        address payer = vm.addr(1);
        string memory id = "9f615c06-7da7-4c65-aef7-946fa073b6b3";
        uint256 amount = 1000;
        address token = usdcMainnetAddress;
        // block number has already passed
        uint256 expiryBlock = block.number - 100;

        // create invoice signature
        bytes memory hash = abi.encode(payer, id, amount, token, expiryBlock);
        bytes32 signatureHash =
            keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n", vm.toString(hash.length), hash));
        (uint8 v, bytes32 r, bytes32 s) = vm.sign(INVOICE_ISSUER_PK, signatureHash);

        vm.startPrank(payer);
        vm.expectRevert(InvoiceExpired.selector);
        stableInvoice.payInvoice(id, amount, token, expiryBlock, r, s, v);
    }

    function testUnsupportedTokenRevert() public {
        vm.selectFork(mainnetFork);

        stableInvoice = new StableInvoice(
            invoiceIssuer, // invoice issuing address
            invoiceIssuer, // payout address
            usdcMainnetAddress,
            usdtMainnetAddress 
        );

        uint256 INVOICE_ISSUER_PK = vm.envUint("INVOICE_ISSUER_PK");

        // address that will pay the invoiced amount
        address payer = vm.addr(1);
        string memory id = "9f615c06-7da7-4c65-aef7-946fa073b6b3";
        uint256 amount = 1000;
        address token = vm.addr(2); // NOT the address of USDC or USDT
        uint256 expiryBlock = block.number + 100;

        // create invoice signature
        bytes memory hash = abi.encode(payer, id, amount, token, expiryBlock);
        bytes32 signatureHash =
            keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n", vm.toString(hash.length), hash));
        (uint8 v, bytes32 r, bytes32 s) = vm.sign(INVOICE_ISSUER_PK, signatureHash);

        vm.startPrank(payer);
        vm.expectRevert(UnsupportedToken.selector);
        stableInvoice.payInvoice(id, amount, token, expiryBlock, r, s, v);
    }
}
